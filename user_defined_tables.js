/**
 * @file
 * Javascript functionality for table weights.
 */

(function ($) {
  'use strict';

  Drupal.behaviors.user_defined_tables = {
    attach: function (context, settings) {

      // Do nothing we have nothing.
      if ($('.user-defined-table').length) {
        // Sorting functionality.
        $('.user-defined-tables-sortable').click(function (e) {

          var id = $(this).parents('.user-defined-table').first().data('table-id');

          if ($('#sort_column_' + id).val() === $(this).data('column-id').toString()) {
            $('#sort_direction_' + id).val($('#sort_direction_' + id).val() === 'DESC' ? 'ASC' : 'DESC');
          }
          else {
            $('#sort_column_' + id).val($(this).data('column-id'));
          }
          e.preventDefault();
          $(this).parents('form').first().submit();
        });

        // Paging functionality.
        $('.user-defined-tables-pager a').click(function (e) {
          var id = $(this).parents('.user-defined-table').first().data('table-id');
          var s = $(this).data('start');
          $('#start_' + id).val(s);

          e.preventDefault();
          $(this).parents('form').first().submit();
        });

        // Search again.
        $('.user-defined-tables-search').click(function (e) {
          var id = $(this).parents('.user-defined-table').first().data('table-id');
          $('#submit_' + id).val('');
          $('#start_' + id).val(0);
          e.preventDefault();
          $(this).parents('form').first().submit();
        });
      }
    }
  };
}(jQuery));
