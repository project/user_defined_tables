CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended Modules
 * Installation
 
INTRODUCTION
------------

The User Defined Tables module allows a user to create
tabular output that is searchable from the front end. The columns
and rows can be edited manually or imported from a CSV file.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/shaunfrisbee/2466065

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2466065
   
REQUIREMENTS
------------

This module requires the following modules:

 * Entity API (https://www.drupal.org/project/entity)
 * Token (https://www.drupal.org/project/token)
 * Token Filter
   (https://ftp.drupal.org/files/projects/token_filter-7.x-1.1.tar.gz)

RECOMMENDED MODULES
-------------------

 * Markdown filter (https://www.drupal.org/project/markdown):
   When enabled, display of the project's README.md help will be rendered with
   markdown.

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
