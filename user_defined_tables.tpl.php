<?php

/**
 * @file
 * The theme file for table output.
 */
    $fp = floor($start / $limit) - 4;
    $lp = floor($start / $limit) + 5;

    // The pager shouldn't end after the end.
    if($lp > floor($count / $limit)) {
        $fp -= ($lp - floor($count / $limit));
    }

    // The pager can't start before 0.
    if($fp < 0) {
        $lp += $fp * -1;
        $fp = 0;
    }

?>

  <div class="user-defined-table" data-table-id="<?php print $table->id; ?>">
    <?php if($table->view == 0): ?>
    <ul>
      <li><a href="">New Search</a></li>
      <li><a href="#" class="user-defined-tables-search">Search Again</a></li>
    </ul>
    <?php endif; ?>

    <?php if($count > 0): ?>
    <p style="font-weight: bold;">Results
      <?php print $start + 1; ?>-
      <?php print $start + count($rows); ?> of
      <?php print $count; ?>
    </p>

    <?php if($count > $limit): ?>

    <div class="user-defined-tables-pager">
      <?php if($start > 0): ?>
      <a href="#" data-start="0" class="user-defined-tables-first">&lt;&lt;</a>
      <a href="#" data-start="<?php print ($start - $limit); ?>" class="user-defined-tables-prev">&lt;</a>
      <?php endif; ?>

      <?php $i = 0; while($i * $limit < $count): ?>

      <?php if($i >= $fp && $i <= $lp): ?>
      <?php if($i * $limit == $start): ?>
      <span data-start="<?php print ($i * $limit); ?>" class="user-defined-tables-page"><?php print ($i + 1); ?></span>
      <?php else: ?>
      <a href="#" data-start="<?php print ($i * $limit); ?>" class="user-defined-tables-page">
        <?php print ($i + 1); ?>
      </a>
      <?php endif; ?>
      <?php endif; ?>

      <?php $i++; ?>
      <?php endwhile; ?>

      <?php if(($start + $limit) < $count): ?>
      <a href="#" data-start="<?php print ($start + $limit); ?>" class="user-defined-tables-next">&gt;</a>
      <a href="#" data-start="<?php print floor($count / $limit) * $limit; ?>" class="user-defined-tables-last">&gt;&gt;</a>
      <?php endif; ?>
    </div>

    <?php endif; ?>

    <table data-table-id="<?php print $table->id; ?>">
      <thead>
        <?php foreach($cols as $col): ?>
        <th class="<?php print $col->sortable == 1 ? " user-defined-tables-sortable " : " "; ?>  <?php print $col->class; ?>" data-column-id="<?php print $col->id; ?>">
          <?php print $col->name; ?>
        </th>
        <?php endforeach; ?>
      </thead>
      <tbody>
        <?php foreach($rows as $row): ?>
        <tr>
          <?php foreach($cols as $col): ?>
          <td class="<?php print $col->type === 'num' || $col->type === 'date' ? 'user-defined-tables-alignright' : ''; ?>">
            <?php switch($col->type): case 'bool': ?>
            <?php print empty($row[$col->name]) || $row[$col->name] == 0 ? 'No' : 'Yes'; ?>
              <?php break;

 case 'date': ?>
            <?php
                                        if(!empty($row[$col->name])) {
                                            print format_date(strtotime($row[$col->name]), 'custom', 'm/d/Y');
                                        } else {
                                            print '&nbsp;';
                                        }
                                    ?>
   <?php break;
            case 'num': ?>
            <?php print floatval($row[$col->name]); ?>
    <?php break;

 default:
                          if(empty($row[$col->name])): ?> &nbsp;
              <?php elseif(preg_match('/^http:\/\/[^\s]*$/', $row[$col->name])): ?>
              <a href="<?php print $row[$col->name]; ?>" target="_blank">Link</a>

              <?php else: ?>
              <?php print $row[$col->name]; ?>
              <?php endif; ?>
   <?php break; ?>
            <?php endswitch; ?>
          </td>
          <?php endforeach; ?>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>

    <?php if($count > $limit): ?>

    <div class="user-defined-tables-pager">
      <?php if($start > 0): ?>
      <a href="#" data-start="0" class="user-defined-tables-first">&lt;&lt;</a>
      <a href="#" data-start="<?php print ($start - $limit); ?>" class="user-defined-tables-prev">&lt;</a>
      <?php endif; ?>

      <?php $i = 0; while($i * $limit < $count): ?>

      <?php if($i >= $fp && $i <= $lp): ?>
      <?php if($i * $limit == $start): ?>
      <span data-start="<?php print ($i * $limit); ?>" class="user-defined-tables-page"><?php print ($i + 1); ?></span>
      <?php else: ?>
      <a href="#" data-start="<?php print ($i * $limit); ?>" class="user-defined-tables-page">
        <?php print ($i + 1); ?>
      </a>
      <?php endif; ?>
      <?php endif; ?>

      <?php $i++; ?>
      <?php endwhile; ?>

      <?php if(($start + $limit) < $count): ?>
      <a href="#" data-start="<?php print ($start + $limit); ?>" class="user-defined-tables-next">&gt;</a>
      <a href="#" data-start="<?php print floor($count / $limit) * $limit; ?>" class="user-defined-tables-last">&gt;&gt;</a>
      <?php endif; ?>
    </div>

    <?php endif; ?>
    <?php else: ?>
    <p>No results found.</p>
    <?php endif; ?>
  </div>
